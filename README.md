proftpd-mod_tar
===============

Status
------
[![Build Status](https://travis-ci.org/Castaglia/proftpd-mod_tar.svg?branch=master)](https://travis-ci.org/Castaglia/proftpd-mod_tar)
[![License](https://img.shields.io/badge/license-GPL-brightgreen.svg)](https://img.shields.io/badge/license-GPL-brightgreen.svg)


Synopsis
--------
The `mod_tar` module for ProFTPD provides for on-the-fly creation of
tarballs for downloaded directories.

For further module documentation, see [mod_tar.html](https://htmlpreview.github.io/?https://github.com/Castaglia/proftpd-mod_tar/blob/master/mod_tar.html).
